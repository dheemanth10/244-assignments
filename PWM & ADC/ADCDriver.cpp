#include "ADCDriver.hpp"
#include "LPC17xx.h"
#include "stdio.h"

// Nothing needs to be done within the default constructor
ADCDriver::ADCDriver(){}

/**
 * 1) Powers up ADC peripheral
 * 2) Set peripheral clock
 * 2) Enable ADC
 * 3) Select ADC channels
 * 4) Enable burst mode
 */
void ADCDriver::adcInitBurstMode()
{
    LPC_SC->PCONP |=(1 <<12); //Set pcadc in pcon

    LPC_ADC->ADCR |=(1<<21); //set pdn in Ad0cr

    LPC_SC->PCLKSEL0 |=(3 <<24); //pclk_adc

    LPC_ADC->ADCR |= (1<<16); //select burst mode

    LPC_ADC->ADCR &= ~(7<<24); //start




}

/**
 * 1) Selects ADC functionality of any of the ADC pins that are ADC capable
 *
 * @param adc_pin_arg is the ADC_PIN enumeration of the desired pin.
 *
 * WARNING: For proper operation of the SJOne board, do NOT configure any pins
 *           as ADC except for 0.26, 1.31, 1.30
 */
void ADCDriver::adcSelectPin(ADC_PIN adc_pin_arg)
{
    switch(adc_pin_arg)
    {
        case(ADC_PIN_0_25):LPC_PINCON->PINSEL1 &=~(3 << 18); //reset
        LPC_PINCON->PINSEL1 |=(01 << 18); //set 0.2
        LPC_ADC->ADCR |=(1<<2); //3rd channel

        break;

        case(ADC_PIN_0_26):LPC_PINCON->PINSEL1 &=~(3 << 20); //reset
        LPC_PINCON->PINSEL1 |=(01 << 20); //set 0.3
        LPC_ADC->ADCR |=(1<<3); //3rd channel

        break;

        case(ADC_PIN_1_30):LPC_PINCON->PINSEL3 &=~(3 << 28); //reset
        LPC_PINCON->PINSEL3 |=(3 << 28); //set 0.4
        LPC_ADC->ADCR |=(1<<4); //3rd channel

        break;

        case(ADC_PIN_1_31):LPC_PINCON->PINSEL3 &=~(3 << 30); //reset
        LPC_PINCON->PINSEL3 |=(3 << 30); //set 0.5
        LPC_ADC->ADCR |=(1<<5); //3rd channel

        break;
    }


}

/**
 * 1) Returns the voltage reading of the 12bit register of a given ADC channel
 *
 * @param adc_channel_arg is the number (0 through 7) of the desired ADC channel.
 */

float ADCDriver::readADCVoltageByChannel(uint8_t adc_channel_arg)
{
    float result=0;
    switch(adc_channel_arg)
    {
        case(2):
                            if( LPC_ADC ->ADSTAT & (1<<2) )
                            {

                            result = (((LPC_ADC->ADGDR ) & 0xFFFF) >>4 );
                            return result;

                            }
        break;
        case(3):
                            if( LPC_ADC ->ADSTAT & (1<<3) )
                            {
                                result = (((LPC_ADC->ADDR3 ) & 0xFFFF) >>4 );
                                return result;
                            }
        break;
        case(3):
                            if( LPC_ADC ->ADSTAT & (1<<4) )
                            {
                                result = (((LPC_ADC->ADDR4 ) & 0xFFFF) >>4 );
                                return result;
                            }
        break;
        case(3):
                            if( LPC_ADC ->ADSTAT & (1<<5) )
                            {
                                result = (((LPC_ADC->ADDR5 ) & 0xFFFF) >>4 );
                                return result;
                            }
        break;

    }



}


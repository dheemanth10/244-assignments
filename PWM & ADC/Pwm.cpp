#include "Pwm.hpp"
#include "LPC17xx.h"
#include "sys_config.h"
#include "stdio.h"

/// Nothing needs to be done within the default constructor
    PWMDriver::PWMDriver()
    {
        frequency=0;
    }
    //PWMDriver::~PWMDriver() {}

/**
 * 1) Select PWM functionality on all PWM-able pins.
 */
void PWMDriver::pwmSelectAllPins()
{
    //Clearing Bits
    LPC_PINCON->PINSEL4 &= ~(3 << 0);
    LPC_PINCON->PINSEL4 &= ~(3 << 2);
    LPC_PINCON->PINSEL4 &= ~(3 << 4);
    LPC_PINCON->PINSEL4 &= ~(3 << 6);
    LPC_PINCON->PINSEL4 &= ~(3 << 8);
    LPC_PINCON->PINSEL4 &= ~(3 << 10);

    //Set Bits for PWM
    LPC_PINCON->PINSEL4 |=(01 << 0);
    LPC_PINCON->PINSEL4 |=(01 << 2);
    LPC_PINCON->PINSEL4 |=(01 << 4);
    LPC_PINCON->PINSEL4 |=(01 << 6);
    LPC_PINCON->PINSEL4 |=(01 << 8);
    LPC_PINCON->PINSEL4 |=(01 << 10);
}

/**
 * 1) Select PWM functionality of pwm_pin_arg
 *
 * @param pwm_pin_arg is the PWM_PIN enumeration of the desired pin.
 */
void PWMDriver::pwmSelectPin(PWM_PIN pwm_pin_arg)
{

    switch(pwm_pin_arg)
    {
        case( PWM_PIN_2_0):     LPC_PINCON->PINSEL4 &= ~(3 << 0); // PWM1.1
        LPC_PINCON->PINSEL4 |=(01 << 0);
        break;
        case( PWM_PIN_2_1):     LPC_PINCON->PINSEL4 &= ~(3 << 2); // PWM1.2
        LPC_PINCON->PINSEL4 |=(01 << 2);
        break;
        case( PWM_PIN_2_2):     LPC_PINCON->PINSEL4 &= ~(3 << 4); // PWM1.3
        LPC_PINCON->PINSEL4 |=(01 << 4);
        break;
        case( PWM_PIN_2_3):     LPC_PINCON->PINSEL4 &= ~(3 << 6); // PWM1.4
        LPC_PINCON->PINSEL4 |=(01 << 6);
        break;
        case( PWM_PIN_2_4):     LPC_PINCON->PINSEL4 &= ~(3 << 8); // PWM1.5
        LPC_PINCON->PINSEL4 |=(01 << 8);
        break;
        case( PWM_PIN_2_5):     LPC_PINCON->PINSEL4 &= ~(3 << 10); // PWM1.6
        LPC_PINCON->PINSEL4 |=(01 << 10);
        break;

    }

}

/**
 * Initialize your PWM peripherals.  See the notes here:
 * http://books.socialledge.com/books/embedded-drivers-real-time-operating-systems/page/pwm-%28pulse-width-modulation%29
 *
 * In general, you init the PWM peripheral, its frequency, and initialize your PWM channels and set them to 0% duty cycle
 *
 * @param frequency_Hz is the initial frequency in Hz.
 */

void PWMDriver::pwmInitSingleEdgeMode(uint32_t frequency_Hz)
{

    LPC_SC->PCONP |= (1 << 6); //Enable pwm bit

    LPC_SC->PCLKSEL0 |= (3 << 12 ); // Enalbe Pwm1 in Pclk

    //Enable all 6 channels , ie bit 9-14
    LPC_PWM1->PCR &= ~(0x3F <<9);
    LPC_PWM1->PCR |=  (0x3F <<9);

    // Reset when TC matches PWMMR0
    LPC_PWM1->MCR |= (1 << 1);

    //Set Frequency in Hz
    frequency = (sys_get_cpu_clock() / frequency_Hz); // or just set 96Mhz as clock cpu
    LPC_PWM1->MR0 = frequency;

    //Enable Counter and PWM
    LPC_PWM1->TCR |=(1 <<0) | (1<<3);

    //Not setting CTCR
    LPC_PWM1->CTCR &= ~(0xF << 0);

    //Set all channels to 0 duty cycle
    LPC_PWM1->MR1 = 0;
    LPC_PWM1->MR2 = 0;
    LPC_PWM1->MR3 = 0;
    LPC_PWM1->MR4 = 0;
    LPC_PWM1->MR5 = 0;
    LPC_PWM1->MR6 = 0;
}

/**
 * 1) Convert duty_cycle_percentage to the appropriate match register value (depends on current frequency)
 * 2) Assign the above value to the appropriate MRn register (depends on pwm_pin_arg)
 *
 * @param pwm_pin_arg is the PWM_PIN enumeration of the desired pin.
 * @param duty_cycle_percentage is the desired duty cycle percentage.
 */
void PWMDriver::setDutyCycle(PWM_PIN pwm_pin_arg, float duty_cycle_percentage)
{
       int duty_cycle = ((duty_cycle_percentage * frequency)/100 );
 //   int duty_cycle = ((duty_cycle_percentage * 100)/frequency );
    //printf("%d\n",duty_cycle);
       switch(pwm_pin_arg)
       {

           case PWM_PIN_2_0: LPC_PWM1->MR1 = duty_cycle;  break;
           case PWM_PIN_2_1: LPC_PWM1->MR2 = duty_cycle;  break;
           case PWM_PIN_2_2: LPC_PWM1->MR3 = duty_cycle;  break;
           case PWM_PIN_2_3: LPC_PWM1->MR4 = duty_cycle;  break;
           case PWM_PIN_2_4: LPC_PWM1->MR5 = duty_cycle;  break;
           case PWM_PIN_2_5: LPC_PWM1->MR6 = duty_cycle;  break;

       }

       LPC_PWM1->LER |= (1 << (pwm_pin_arg+1));
}

/**
 * Optional:
 * 1) Convert frequency_Hz to the appropriate match register value
 * 2) Assign the above value to MR0
 *
 * @param frequency_hz is the desired frequency of all pwm pins
 */
void PWMDriver::setFrequency(uint32_t frequency_Hz)
{
    //Set Frequency in Hz
    frequency = (sys_get_cpu_clock() / frequency_Hz); // or just set 96Mhz as clock cpu
    LPC_PWM1->MR0 = frequency;

}

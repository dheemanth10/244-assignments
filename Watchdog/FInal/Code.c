#include "stdio.h"
#include "sys_config.h"
#include "stdlib.h"
#include <iostream>
#include "utilities.h"
#include "io.hpp"
#include "storage.hpp"
#include "FreeRTOS.h"
#include "LPC17xx.h"
#include "task.h"
#include "uart0.hpp"
#include "LabGPIO_0.hpp"
#include "LabGPIOInterrupts.hpp"
#include "LabSPI.hpp"
#include "ADCDriver.hpp"
#include "Pwm.hpp"
#include "io.hpp"
#include "LabUart.hpp"
#include "queue.h"
#include <time.h>
#include "event_groups.h"
#include "printf_lib.h"
#include "i2c2.hpp"
#include "rtc.h"

using namespace std;


const uint32_t PRO_BIT   ( 1 << 0 );
const uint32_t CON_BIT   ( 1 << 1 );
const uint32_t BOTH_BITS = (PRO_BIT | CON_BIT);
EventGroupHandle_t xEventGroup;


QueueHandle_t ls;
int count=0;
int ls_average=0;

void producer_task(void *p)
{
    while (1)
    {
        ls_average+= LS.getPercentValue();
        count++;

        if(count%100 == 0)
        {

            ls_average /=100;
            xQueueSend(ls,  &ls_average, 0);
            //file_write(ls_average);

            LD.setNumber(ls_average);
            ls_average=0;

        }

      xEventGroupSetBits(xEventGroup,PRO_BIT);




    }


}

int x[10];
void consumer_task(void *p ) /* HIGH priority */
{
    int i=0;
    while (1)
    {
        xQueueReceive(ls, &x[i], portMAX_DELAY);
        // u0_dbg_printf("Message Recieved from Queue \n");
        i++;
        if(i==10)
            {i =0;}
        xEventGroupSetBits(xEventGroup,CON_BIT);

    }

}

void watchdog_task(void *p)
{
    while(1)
    {
        uint32_t WD = xEventGroupWaitBits(xEventGroup,BOTH_BITS,pdTRUE,pdTRUE,1000);
        FILE *fd ;
        fd = fopen("1:stuck.txt", "a+");
        if((WD & BOTH_BITS) == BOTH_BITS )
        {
            //do ntn
            fprintf(fd,"%s - Both Tasks Working \n\n",rtc_get_date_time_str());
            // u0_dbg_printf("Both Tasks Working \n");
        }

        else
        {
        if(!(WD & PRO_BIT))
        {
            //write that pro not working
            fprintf(fd,"%s - Producer Task Not Working \n\n",rtc_get_date_time_str());
             u0_dbg_printf("Producer Task Not Working %x\n",WD);
        }
        if(!(WD & CON_BIT))
        {
            //write that con was not working
            fprintf(fd,"%s - Consumer Task Not Working \n\n",rtc_get_date_time_str());
            u0_dbg_printf("Consumer Task Not Working %x\n",WD);
        }
        }

        fclose(fd);

        vTaskDelay(1000);
    }
}
void write_to_file(void *p) /* HIGH priority */
{

    while (1)
    {
        int i = 0;
        FILE *fd ;
        fd = fopen("1:Light Sensor Average.csv", "a+");
        while(i<=9)
        {
           fprintf(fd,"Light Sensor Average Value = , %d , at , %s \n",x[i],rtc_get_date_time_str());


            i++;
        }

        fclose(fd);

        vTaskDelay(1000);
    }
}

TaskHandle_t xProducer_Task = NULL;
TaskHandle_t xConsumer_Task = NULL;

CMD_HANDLER_FUNC(watchproducer)
{

    // You can use FreeRTOS API or the wrapper resume() or suspend() methods
    if (cmdParams == "on") {

        vTaskResume(xProducer_Task);
        u0_dbg_printf("\n Producer Task Resumed \n");
    }
    else {

        vTaskSuspend(xProducer_Task);
        u0_dbg_printf("\n Producer Task Suspended \n");
    }

    return true;
}



CMD_HANDLER_FUNC(watchconsumer)
{

    // You can use FreeRTOS API or the wrapper resume() or suspend() methods
    if (cmdParams == "on") {
        u0_dbg_printf("\n Consumer Task Resumed \n");
        vTaskResume(xConsumer_Task);

    }
    else {
        u0_dbg_printf("\n Consumer Task Suspended \n");
        vTaskSuspend(xConsumer_Task);
    }

    return true;
}

void cpu_status(void *p)
{
    while(1)
    {
        //Taken from handlers.cpp >> taskListHandler
        vTaskDelay(10000);
		vTaskResetRunTimeStats();
        vTaskDelayMs(10);
        FILE *fd ;
        fd = fopen("1:Cpu Status.txt", "a+");
        const char * const taskStatusTbl[] = { "RUN", "RDY", "BLK", "SUS", "DEL" };
        const unsigned portBASE_TYPE maxTasks = 16;
        TaskStatus_t status[maxTasks];
        uint32_t totalRunTime = 0;
        uint32_t tasksRunTime = 0;
        const unsigned portBASE_TYPE uxArraySize =
                uxTaskGetSystemState(&status[0], maxTasks, &totalRunTime);

        fprintf(fd,"Cpu Status at %s \n",rtc_get_date_time_str());
        fprintf(fd,"%10s Sta Pr Stack CPU%%          Time\n", "Name");
        for(unsigned priorityNum = 0; priorityNum < configMAX_PRIORITIES; priorityNum++)
        {
            /* Print in sorted priority order */
            for (unsigned i = 0; i < uxArraySize; i++) {
                TaskStatus_t *e = &status[i];
                if (e->uxBasePriority == priorityNum) {
                    tasksRunTime += e->ulRunTimeCounter;

                    const uint32_t cpuPercent = (0 == totalRunTime) ? 0 : e->ulRunTimeCounter / (totalRunTime/100);
                    const uint32_t timeUs = e->ulRunTimeCounter;
                    const uint32_t stackInBytes = (4 * e->usStackHighWaterMark);

                    fprintf(fd,"%10s %s %2u %5u %4u %10u us\n",
                            e->pcTaskName, taskStatusTbl[e->eCurrentState], e->uxBasePriority,
                            stackInBytes, cpuPercent, timeUs);

                }

            }
        }
        fprintf(fd,"\n");
        fclose(fd);
    }
}

int main(int argc, char const *argv[])
{
    rtc_t timeset;

    timeset.day=10;
    timeset.dow=2;
    timeset.hour=11;
    timeset.min=10;
    timeset.month=4;
    timeset.sec=00;
    timeset.year=2018;

    rtc_settime(&timeset);

    xEventGroup = xEventGroupCreate();

    ls =xQueueCreate(32, sizeof(int));

    scheduler_add_task(new terminalTask(PRIORITY_HIGH));

    xTaskCreate(producer_task,(char*)"producer",1024,0,1,&xProducer_Task);
    xTaskCreate(consumer_task,(char*)"consumer",1024,0,1,&xConsumer_Task);
    xTaskCreate(watchdog_task,(char*)"watchdog",1024,0,2,0);
    xTaskCreate(write_to_file,(char*)"sd write",1024,0,1,0);
    xTaskCreate(cpu_status,(char*)"cpu info",1024,0,1,0);

    scheduler_start();

    vTaskStartScheduler();

    return 0;
}

